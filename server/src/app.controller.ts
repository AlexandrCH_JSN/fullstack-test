import { Controller, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { UserService } from './user.service';
import { GroupService } from './group.service';
import { User as UserModel, Group as GroupModel } from '@prisma/client';

@Controller()
export class AppController {
  constructor(
    private readonly userService: UserService,
    private readonly groupService: GroupService,
  ) {}

  @Post('user')
  async signupUser(
    @Body()
    userData: {
      login: string;
      password: string;
      // userGroups: [];
    },
  ): Promise<UserModel> {
    const { login, password } = userData;
    // const groupIds = Object.values(userGroups).map(({ id }) => id);
    const registration_date = new Date();

    return this.userService.createUser({
      login,
      password,
      registration_date,
      // userGroups: {
      //   connectOrCreate: {

      //   }
      // }
    });
  }

  @Post('group')
  async createGroup(
    @Body()
    groupData: {
      name: string;
    },
  ): Promise<GroupModel> {
    return this.groupService.createGroup(groupData);
  }

  @Delete('group/:id')
  async deleteGroup(@Param('id') id: string): Promise<GroupModel> {
    return this.groupService.deleteGroup({ id: Number(id) });
  }
}
