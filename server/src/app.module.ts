import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PrismaService } from './prisma.service';
import { UserService } from './user.service';
import { GroupService } from './group.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [PrismaService, UserService, GroupService],
})
export class AppModule {}
